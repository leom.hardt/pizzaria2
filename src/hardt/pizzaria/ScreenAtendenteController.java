package hardt.pizzaria;

import com.sun.media.jfxmediaimpl.platform.Platform;
import hardt.pizzaria.model.Pizza;
import hardt.pizzaria.model.PizzaDB;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Léo Hardt
 */
public class ScreenAtendenteController implements Initializable {
    
    @FXML
    TableColumn idColumn, clienteColumn, pizzaColumn, prontaColumn;
    
    @FXML
    TableView<Pizza> table;
    
    @FXML
    Button entregar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table.setItems(FXCollections.observableList(PizzaDB.getAll()));
        
        idColumn.setCellValueFactory(new PropertyValueFactory("idPizza"));
        clienteColumn.setCellValueFactory(new PropertyValueFactory("cliente"));
        pizzaColumn.setCellValueFactory(new PropertyValueFactory("nome"));
        prontaColumn.setCellValueFactory(new PropertyValueFactory("prontaString"));
    }    
    
    public void onAdicionarPizza(){
        Main.mudarDeTela("AddPedido.fxml");
    }
    
    @FXML
    void onSaiMenu(){
        Main.getScene().setCursor(Cursor.DEFAULT);
    }
    
    @FXML
    void onEntraMenu(){
        Main.getScene().setCursor(Cursor.HAND);
    } 
    
    @FXML
    void voltaMenu(){
        Main.mudarDeTela("TelaInicial.fxml");
        
    }
    
    void deletarItemSelecionado(){
        int index = table.getSelectionModel().getSelectedIndex();
        Pizza p = table.getItems().get(table.getSelectionModel().getSelectedIndex());
        System.out.println("Removendo o item " + p);
        table.getItems().remove(p); 
        PizzaDB.delete(p.getIdPizza());
    }
    
    @FXML
    void cliqueMouse(MouseEvent ev){
        if(MouseButton.SECONDARY == (ev.getButton())){
            ContextMenu ctx = new ContextMenu();
            MenuItem it = new MenuItem("Remover");
            it.setOnAction((ActionEvent evt) -> deletarItemSelecionado());
            ctx.getItems().add(it);
            ctx.show(table,ev.getScreenX(), ev.getScreenY());
                
            
        }
    }
}
