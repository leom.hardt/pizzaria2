/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.pizzaria.model;

/**
 *
 * @author aluno
 */
public class Ingrediente {
    private String nome;
    private double quantidade;
    
    @Override
    public String toString(){
        return "Ingrediente[" + nome + ", " + quantidade + "];";
    }
    
    
    public Ingrediente(String nome, double quantidade){
        this.nome = nome;
        this.quantidade = quantidade;
    }
    
}
