/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.pizzaria.model;

/**
 *
 * @author aluno
 */
public class PizzaCalabresa extends Pizza {
    @Override
    public String getReceita(){
        return "Receita Calabresa";
    }
    
    public PizzaCalabresa(String nomeCliente){
        super(nomeCliente);
        this.nome = "Pizza Calabresa";
    }
}
