package hardt.pizzaria.model;

import java.util.List;

/**
 *
 * @author Léo Hardt
 */
public abstract class Pizza {
    /** O nome do cliente que pediu a pizza. */
    String nomeCliente;
    /** O Id do pedido. */
    int idPizza;
    /** O nome da pizza que foi pedida. */
    String nome;
    /** Se ela está assada ou não. */
    boolean assada;
    
    ///** Se ela está assada ou não. */
    //List<Ingrediente> ingredientes;
    
    Pizza(String nomeCliente){
        assada = false;
        this.nomeCliente = nomeCliente;
    }
    
    boolean cozida(){
        return assada == true;
    }
    
//    void addIngrediente(Ingrediente ing){
//        ingredientes.add(ing);
//    }
    public String getNome(){
        return nome;
    }
    
    void setNomeCliente(String nome){
        this.nomeCliente = nome;
    }
    
    public String getCliente(){
        return nomeCliente;
    }
    public String getNomeCliente(){
        return nomeCliente;
    }

    public int getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(int idPizza) {
        this.idPizza = idPizza;
    }

    public boolean isCozida() {
        return assada;
    }

    public void setCozida(boolean cozida) {
        this.assada = cozida;
    }
    
    public String getProntaString(){
        return assada?"Sim":"Não";
    }
    
    abstract String getReceita();
}
