package hardt.pizzaria.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.List;

/**
 * Database Connection toolbox for the project.
 * 
 * @author Léo Hardt
 */
public class PizzaDB {
    
    /**
     * Useful method for not copying code. In the end, all connections are equal.
     */
    private static Connection getConnection() throws SQLException{
        return DriverManager.getConnection("jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE",
                Dados.getUsername(), Dados.getPassword());   
    }
    
    /**
     * Creates the tables in the database.
     * @return true if succeeded.
     */
    public static boolean criarTabela() throws Exception{
        boolean created = false;
        Class.forName("oracle.jdbc.driver.OracleDriver");
        final String CREATE_TABLE = "CREATE TABLE pizza(" 
                + "  id integer primary key,"
                + "  nome varchar2(40)," 
                + "  cliente varchar2(30)," 
                + "  pronta integer" 
                + ");";
        final String CREATE_SEQUENCE = "CREATE SEQUENCE pizza_pedido";
        try(Connection c = getConnection()){
            boolean createdTable = c.prepareStatement(CREATE_TABLE).execute();
            boolean createdSequence = c.prepareStatement(CREATE_SEQUENCE).execute();
            created = createdTable && createdSequence;
        }catch(SQLSyntaxErrorException e){
            // Se não for um erro por criar uma tabela existente
            if(! e.getLocalizedMessage().contains("ORA-00955")){
                created = false;
            }
        }
        return created;
    }
    
    /**
     * Changes the state of a pizza from uncooked to cooked.
     * @return true if succeeded.
     */
    public static boolean assar(Pizza p){
        final String SQL = "UPDATE pizza SET pronta=1 WHERE id =  ?";
        boolean done = false;
        try{
                Connection c = getConnection();
                PreparedStatement st = c.prepareStatement(SQL);
                st.setInt(1, p.getIdPizza());
                done = st.execute();
        } catch(SQLException e){ }
        return done;
    }
    
    /**
     * Tests if it can log in the database.
     * @return true if succeeded.
     */
    public static boolean testLogin(){
        boolean logsIn = false;
        try(
            Connection c = getConnection();
        ){
            logsIn = c.isValid(0);
        } catch(SQLException e){  }
        return logsIn;
    }
    
    /**
     * Removes one item from the table.
     * @return true if succeeded.
     */
    public static boolean delete(int id){ 
        final String SQL = "DELETE FROM pizza WHERE id =  ?";
        boolean done = false;
        try(
            Connection c = getConnection();
            PreparedStatement st = c.prepareStatement(SQL);
        ){
            st.setInt(1, id);
            done =  st.execute();
        } catch(SQLException e){ }
        
        return done;
    }
    
    /**
     * Gets all items in the table. 
     * 
     * @param whereClause if this parameter is empty, the resulting SQL is
     *      "SELECT * FROM TABLE". If, howeverer, this parameter is "id=___",
     *      the SQL turns to "SELECT * FROM TABLE WHERE id=___".
     */
    private static List<Pizza> getAllWhereClause(String whereClause){
        String sql = "SELECT * FROM pizza";
        if(!whereClause.isEmpty()){
            sql += " where " + whereClause;
        }
        List<Pizza> pizzas = new ArrayList<>();
        try(
            Connection c = getConnection();
            ResultSet res = c.prepareStatement(sql).executeQuery();
        ){
            while(res.next()){
                Pizza p = null;
                String nomePizza = res.getString("nome");
                if(nomePizza.equals("Pizza de Milho")){
                    p = new PizzaDeMilho(res.getString("cliente"));
                }else if(nomePizza.equals("Pizza Calabresa")){
                    p = new PizzaCalabresa(res.getString("cliente"));
                }else if(nomePizza.equals("Pizza Portuguesa")){
                    p = new PizzaPortuguesa(res.getString("cliente"));
                } else continue;
                p.setIdPizza(res.getInt("id"));
                p.setCozida(res.getBoolean("pronta"));
                pizzas.add(p);
            }
        }catch(SQLException e){
            System.out.println("Exception!!" + e);
        }
        return pizzas;
    }
   
    /**
     * Returns all the uncooked pizzas in the database.
     * @return all the uncooked pizzas in the database.
     */
    public static List<Pizza> getNaoProntos(){
        return getAllWhereClause("pronta = 0");
    }
   
    /**
     * Returns all the pizzas in the database.
     * @return all the pizzas in the database.
     */
    public static List<Pizza> getAll(){
        return getAllWhereClause("");
    }
    
    /**
     * Inserts one pizza in the database.
     * 
     * @param p the pizza to be inserted.
     * @return if it was inserted.
     */
    public static boolean insert(Pizza p){
        String SQL = "INSERT INTO pizza(id, nome, cliente, pronta) values (pizza_pedido.nextval,?,?,?)";
        boolean inserted = false;
        try(
            Connection c =getConnection();
            PreparedStatement st = c.prepareStatement(SQL);
        ){
            st.setString(1, p.getNome());
            st.setString(2, p.getNomeCliente());
            st.setBoolean(3, false);
            inserted = st.execute();
        } catch(SQLException e){
            System.out.println("Exception e: " + e );
        }
        return inserted;
    }
}
