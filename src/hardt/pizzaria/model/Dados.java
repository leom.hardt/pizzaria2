package hardt.pizzaria.model;

/**
 * @author Léo Hardt
 */
public class Dados {
    private static String username, password;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Dados.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Dados.password = password;
    }
}
