/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.pizzaria.model;

/**
 *
 * @author aluno
 */
public class PizzaPortuguesa extends Pizza{
    @Override
    public String getReceita(){
        return "Receita Portuguesa";
    }
    public PizzaPortuguesa(String nomeCliente){
        super(nomeCliente);
        this.nome = "Pizza Portuguesa";
    }
}
