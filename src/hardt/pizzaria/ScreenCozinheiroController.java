package hardt.pizzaria;

import hardt.pizzaria.model.Pizza;
import hardt.pizzaria.model.PizzaDB;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * @author Léo Hardt
 */
public class ScreenCozinheiroController implements Initializable {
    
    @FXML
    TableView table;
    @FXML
    Button assar;
    @FXML
    TableColumn idColumn, nomeColumn, receitaColumn;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection == null) {
                onDeselect();
            } else {
                onSelected();
            }
        });
        
        idColumn.setCellValueFactory(new PropertyValueFactory("idPizza"));
        nomeColumn.setCellValueFactory(new PropertyValueFactory("nome"));
        receitaColumn.setCellValueFactory(new PropertyValueFactory("receita"));
        
        table.setItems(FXCollections.observableList(PizzaDB.getNaoProntos()));
    }    
    
    public void onSelected(){
        assar.setDisable(false);
    }
    
    public void onDeselect(){
        assar.setDisable(false);
    }
    
    public void onAssar(){
        Pizza selecionada = (Pizza)table.getSelectionModel().getSelectedItem();
        PizzaDB.assar(selecionada);
        table.setItems(FXCollections.observableList(PizzaDB.getNaoProntos()));
    }
    
    @FXML
    void onSaiMenu(){
        Main.getScene().setCursor(Cursor.DEFAULT);
    }
    
    @FXML
    void onEntraMenu(){
        Main.getScene().setCursor(Cursor.HAND);
    } 
    
    @FXML
    void voltaMenu(){
        Main.mudarDeTela("TelaInicial.fxml");
        
    }
}
