/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.pizzaria;

import hardt.pizzaria.model.PizzaCalabresa;
import hardt.pizzaria.model.PizzaDB;
import hardt.pizzaria.model.PizzaDeMilho;
import hardt.pizzaria.model.PizzaPortuguesa;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;

/**
 * FXML Controller class
 *
 * @author aluno
 */
public class AddPedidoController implements Initializable {

    @FXML
    Toggle tgCalabresa, tgDeMilho, tgPortuguesa;
    
    @FXML
    TextField nomeCliente;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void onCadastrar(){
        if(nomeCliente.getText().isEmpty()){
            Dialog<String> dialog = new Dialog<>();
            dialog.setContentText("Nome do cliente em branco!");
            dialog.setTitle("erro!");
            dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
            dialog.showAndWait();
            return;
        }
        
        if(tgCalabresa.isSelected()){
            System.out.println("Calabresa");
            PizzaDB.insert(new PizzaCalabresa(nomeCliente.getText()));
            tgCalabresa.setSelected(false);
        } else if(tgPortuguesa.isSelected()){
            System.out.println("Portuguesa");
            PizzaDB.insert(new PizzaPortuguesa(nomeCliente.getText()));
            tgCalabresa.setSelected(false);
        } else if(tgDeMilho.isSelected()){
            PizzaDB.insert(new PizzaDeMilho(nomeCliente.getText()));
            tgCalabresa.setSelected(false);
        } else {
            Dialog<String> dialog = new Dialog<>();
            dialog.setContentText("Não há tipo de pizza selecionado!");
            dialog.setTitle("erro!");
            dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
            dialog.showAndWait();
            return;
        }
        Main.mudarDeTela("ScreenAtendente.fxml");
        
    }
    
    public void onVoltar(){
        Main.mudarDeTela("ScreenAtendente.fxml");
    }
    
}
