/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.pizzaria;

import hardt.pizzaria.model.Dados;
import hardt.pizzaria.model.PizzaDB;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author aluno
 */
public class LoginController implements Initializable {

    @FXML
    TextField username;
    @FXML
    PasswordField password;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    private void onEntrar(){
        Dados.setPassword(password.getText());
        Dados.setUsername(username.getText());
        if(PizzaDB.testLogin()){
        
            try{
                PizzaDB.criarTabela();
            } catch(Exception e){
                System.out.println("Exception " + e);
            }

            Main.mudarDeTela("TelaInicial.fxml");
        } else {
            
            Dialog<String> d = new Dialog<>();
            d.getDialogPane().setContent(new Label("Senha errada."));
            d.getDialogPane().getButtonTypes().add(ButtonType.OK);
            System.out.println("Senha errada.");   
            d.showAndWait();
        }
    }
}
